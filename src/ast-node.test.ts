import { parse } from "./parser";
import { lex } from "./lexer";
import { ASTNode, traverse } from "./ast-node";
import { getErrors } from "./error";

describe("AST Node", () => {
    describe("Traversal", () => {
        it("Can traverse a node's children", () => {
            const tokens = lex(`a={c:number(1)} b=string("default")`);
            const node = parse(tokens);
            expect(getErrors()).toHaveLength(0);
            const results: string[] = [];
            traverse(
                node,
                (n: ASTNode) => {
                    results.push("Entered " + n.type);
                },
                (n: ASTNode) => {
                    results.push("Exited " + n.type);
                }
            );
            expect(results).toEqual([
                "Entered File",
                "Entered Declaration",
                "Entered Object",
                "Entered Field",
                "Entered Number",
                "Exited Number",
                "Exited Field",
                "Exited Object",
                "Exited Declaration",
                "Entered Declaration",
                "Entered String",
                "Exited String",
                "Exited Declaration",
                "Exited File",
            ]);
        });
    });
});
