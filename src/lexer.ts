import { Token } from "./token";
import {
    getErrors,
    GMLError,
    InvalidTokenError,
    pushError,
    UnterminatedCommentError,
    UnterminatedStringError,
} from "./error";
import {
    advance,
    advanceIf,
    createStream,
    isFinished,
    peek,
    test,
    testAhead,
} from "./stream";
import { TokenType } from "./token-type";
import { Reserved } from "./reserved";

function isDigit(char: string) {
    return char.match(/[0-9]/) !== null;
}

function isAlpha(char: string) {
    return char.match(/[A-Za-z_]/) !== null;
}

export function lex(input: string): Token[] {
    if (input.length === 0) throw Error("Empty input");

    // State
    const stream = createStream(input);
    const tokens: Token[] = [];
    let start = 0;

    // Helpers
    const createToken = (type: TokenType, str?: string) => {
        if (type === undefined)
            throw Error("Undefined is not a valid token type");
        tokens.push({
            type: type,
            offset: start,
            str: str === undefined ? getString() : str,
        });
    };

    const getString = () => input.substring(start, stream.current);

    // Multi char tokens
    const number = () => {
        while (test(stream, isDigit)) advance(stream);
        if (test(stream, (t) => t === ".") && testAhead(stream, 1, isDigit)) {
            advance(stream);
            while (test(stream, isDigit)) advance(stream);
        }
        createToken(TokenType.NUM);
    };

    const boundedToken = (
        boundaryChar: string,
        type: TokenType,
        terminationError: new (offset: number) => GMLError
    ) => {
        while (!test(stream, (t) => t === boundaryChar) && !isFinished(stream))
            advance(stream);

        // Unterminated error
        if (isFinished(stream)) {
            pushError(new terminationError(stream.current));
            return;
        }

        // Consume the closing char
        advance(stream);

        // Trim the boundary characters
        createToken(type, input.substring(start + 1, stream.current - 1));
    };

    const string = () =>
        boundedToken('"', TokenType.STR, UnterminatedStringError);

    const comment = () => {
        // get rid of the additional <
        advanceIf(
            stream,
            (c) => c === "<",
            (c) =>
                pushError(
                    new InvalidTokenError(
                        `Expected a < to complete a comment start but got a ${c}`,
                        stream.current
                    )
                )
        );

        const closedPredicate = (c: string) => c === ">";

        while (
            !isFinished(stream) &&
            !(
                test(stream, closedPredicate) &&
                testAhead(stream, 1, closedPredicate)
            )
        )
            advance(stream);

        // Unterminated error
        if (isFinished(stream)) {
            pushError(new UnterminatedCommentError(stream.current));
            return;
        }

        for (let i = 0; i < 2; i++) {
            if (test(stream, (c) => c === ">")) {
                advance(stream);
            } else {
                if (isFinished(stream)) {
                    pushError(
                        new InvalidTokenError(
                            `Expected a > to complete a comment end but file unexpectedly ended`,
                            stream.current
                        )
                    );
                } else {
                    pushError(
                        new InvalidTokenError(
                            `Expected a > to complete a comment end but got a ${peek(
                                stream
                            )}`,
                            stream.current
                        )
                    );
                }
            }
        }

        // Trim the boundary characters
        createToken(
            TokenType.COMMENT,
            input.substring(start + 2, stream.current - 2)
        );
    };

    const identifier = () => {
        while (test(stream, isAlpha) || test(stream, isDigit)) advance(stream);
        const str = getString();
        if (str === Reserved.True || str === Reserved.False) {
            createToken(TokenType.BOOL, str);
        } else if (str === Reserved.Id) {
            createToken(TokenType.ID, str);
        } else {
            createToken(TokenType.IDENTIFIER, str);
        }
    };

    while (!isFinished(stream)) {
        start = stream.current;
        const c: string = advance(stream);
        switch (c) {
            // IGNORED
            case " ":
            case "\t":
            case "\r":
            case "\n":
                break;

            // SINGLE CHAR
            case "(":
                createToken(TokenType.OPEN_PAREN);
                break;
            case ")":
                createToken(TokenType.CLOSED_PAREN);
                break;
            case "{":
                createToken(TokenType.OPEN_BRACE);
                break;
            case "}":
                createToken(TokenType.CLOSED_BRACE);
                break;
            case "[":
                createToken(TokenType.OPEN_BRACKET);
                break;
            case "]":
                createToken(TokenType.CLOSED_BRACKET);
                break;
            case ":":
                createToken(TokenType.COLON);
                break;
            case "=":
                createToken(TokenType.EQUALS);
                break;
            case ",":
                createToken(TokenType.COMMA);
                break;
            case ".":
                createToken(TokenType.PERIOD);
                break;
            case "*":
                createToken(TokenType.STAR);
                break;
            case "?":
                createToken(TokenType.QUESTION);
                break;
            default:
                // Multi char
                if (c === "<") {
                    comment();
                } else if (c === '"') {
                    string();
                } else if (isDigit(c)) {
                    number();
                } else if (isAlpha(c)) {
                    identifier();
                } else {
                    pushError(
                        new InvalidTokenError(
                            `Unrecognized character: ${c}`,
                            stream.current
                        )
                    );
                }
        }
    }

    if (tokens.length === 0 && getErrors().length === 0)
        throw Error("Empty input");
    tokens.push({ type: TokenType.EOF, str: "EOF", offset: stream.current });
    return tokens;
}
