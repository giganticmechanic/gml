import { clearErrors, getErrorInfo, getErrors } from "./error";
import { lex } from "./lexer";
import { parse } from "./parser";
import * as fs from "fs";
import * as path from "path";

const input = fs
    .readFileSync(path.join(__dirname, "../assets/test.gml"))
    .toString();

describe("The whole thing", () => {
    beforeEach(clearErrors);
    it("works", () => {
        const tokens = lex(input);
        if (getErrors().length) {
            console.log("Lexing Errors:");
            getErrorInfo(getErrors(), input).forEach(e => console.log(e));
        }
        expect(getErrors()).toHaveLength(0);
        const file = parse(tokens);
        if (getErrors().length) {
            console.log("Parsing Errors:");
            getErrorInfo(getErrors(), input).forEach(e => console.log(e));
        }
        expect(getErrors()).toHaveLength(0);
    });
});
