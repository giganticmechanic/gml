export const Reserved = {
    True: "true",
    False: "false",
    Id: "id",
    Bool: "bool",
    Number: "number",
    String: "string",
    Url: "url",
    Video: "video",
    Image: "image",
    Sound: "sound",
    Option: "option",
    List: "list",
    Map: "map",
    Set: "set",
};

function invert(o: { [name: string]: string }) {
    const result: { [name: string]: string } = {};
    Object.keys(o).forEach((key) => {
        const val = o[key];
        result[val] = key;
    });
    return result;
}

const ReservedByValue = invert(Reserved);

export const isReserved = (str: string): Boolean => {
    return ReservedByValue[str] !== undefined;
};
