import {
    ASTNode,
    Declaration,
    Field,
    File,
    GMLMap,
    GMLSet,
    Id,
    List,
    Literal,
    NodeType,
    Obj,
    Option,
    Primitive,
    Range,
    StaticIs,
    Value,
    VarName,
} from "./ast-node";

export const resolve = <T>(val: T | VarName): Value | T => {
    if (VarName.is(val)) return val.value;
    return val;
};

class RenderContext {
    scopeStack = new Stack<BranchNode>();

    get current(): RenderNode {
        const scope = this.scopeStack.peek();
        if (scope === undefined) throw Error("Scope stack unexpectedly empty.");
        return scope;
    }

    push(node: RenderNode) {
        this.current.addChild(node);
        if (node instanceof BranchNode || node instanceof NamedNode) {
            this.scopeStack.push(node);
        }
    }

    pop() {
        this.scopeStack.pop();
    }

    readonly root = new RootNode();

    constructor() {
        this.scopeStack.push(this.root);
    }
}

abstract class RenderNode {
    readonly children: RenderNode[] = [];
    constructor(
        readonly astNode: ASTNode | null,
        readonly parent: RenderNode | null,
        readonly value: NonNullable<any>
    ) {
        if (value === undefined) throw Error("OOPS");
    }

    addChild(child: RenderNode) {
        this.children.push(child);
    }

    write(out: Record<string, NonNullable<any>> | Array<NonNullable<any>>) {
        if (this instanceof RootNode) {
            (out as any).push(this.value);
        }
        // write out self (ignore root and names)
        if (!(this instanceof NamedNode || this instanceof RootNode)) {
            if (Array.isArray(out)) {
                // parent must not be named
                if (this.parent instanceof NamedNode)
                    throw Error(
                        "Children of named parent cannot be added to array"
                    );
                // append it
                out.push(this.value);
            } else {
                // parent must be named
                if (this.parent instanceof NamedNode) {
                    out[this.parent.name] = this.value;
                } else {
                    throw Error(
                        `Must have named parent to be added to object: ${JSON.stringify(
                            this.value
                        )} ${this.parent?.value} ${
                            this.parent instanceof RootNode
                        }`
                    );
                }
            }
        }

        // write children
        if (this instanceof BranchNode) {
            out = this.value;
        }
        this.children.forEach((child) => child.write(out));
    }
}

class LeafNode extends RenderNode {
    constructor(node: ASTNode, parent: RenderNode, value: NonNullable<any>) {
        super(node, parent, value);
    }
}

class NamedNode extends LeafNode {
    get name(): string {
        return this.value;
    }
    constructor(node: ASTNode, parent: RenderNode, name: string) {
        super(node, parent, name);
    }
}

abstract class BranchNode extends RenderNode {}

class ListNode extends BranchNode {
    constructor(node: ASTNode, parent: RenderNode) {
        super(node, parent, []);
    }
}

class ObjNode extends BranchNode {
    constructor(node: ASTNode, parent: RenderNode) {
        super(node, parent, {});
    }
    addChild(child: RenderNode) {
        if (!(child instanceof NamedNode)) {
            throw Error("All children of an object node must be named nodes");
        }
        super.addChild(child);
    }
}

class RootNode extends BranchNode {
    constructor() {
        super(null, null, {});
    }
}

export const generateRenderTree = (file: File): RenderNode => {
    const ctx = new RenderContext();

    const visit = (node: ASTNode, ctx: RenderContext) => {
        // enter
        switch (node.type) {
            case NodeType.Bool:
            case NodeType.Number:
            case NodeType.String:
            case NodeType.Sound:
            case NodeType.Image:
            case NodeType.Video:
            case NodeType.Url:
                ctx.push(
                    new LeafNode(
                        node,
                        ctx.current,
                        (node as Primitive).defaultValue
                    )
                );
                break;
            // PRIMITIVE LITERALS
            case NodeType.BoolLiteral:
            case NodeType.NumLiteral:
            case NodeType.StrLiteral:
                ctx.push(
                    new LeafNode(node, ctx.current, (node as Literal).value)
                );
                break;
            // COMPOSITE
            case NodeType.Object:
                assertIs(Obj, node);
                ctx.push(new ObjNode(node, ctx.current));
                break;
            case NodeType.List:
                assertIs(List, node);
                ctx.push(new ListNode(node, ctx.current));
                {
                    const t = resolve(node.typeParam);
                    let n = size(node);
                    for (let i = 0; i < n; i++) {
                        if (Option.is(t)) {
                            ctx.push(
                                new LeafNode(
                                    node,
                                    ctx.current,
                                    optionName(t, i)
                                )
                            );
                        } else {
                            visit(t, ctx);
                        }
                    }
                }
                break;
            case NodeType.Map:
                assertIs(GMLMap, node);
                ctx.push(new ObjNode(node, ctx.current));
                {
                    if (Option.is(node.keyType)) {
                        const option = node.keyType;
                        let n = size(option.list);
                        for (let i = 0; i < n; i++) {
                            ctx.push(
                                new NamedNode(
                                    node,
                                    ctx.current,
                                    optionName(option, i)
                                )
                            );
                            visit(node.valueType, ctx);
                            ctx.pop(); // pop the named node
                        }
                    } else {
                        ctx.push(
                            new NamedNode(
                                node,
                                ctx.current,
                                node.keyType.defaultValue
                            )
                        );
                        visit(node.valueType, ctx);
                        ctx.pop();
                    }
                }
                break;
            // MISC
            case NodeType.Declaration:
                assertIs(Declaration, node);
                ctx.push(new NamedNode(node, ctx.current, node.identifier.str));
                break;
            case NodeType.Field:
                assertIs(Field, node);
                ctx.push(new NamedNode(node, ctx.current, node.identifier.str));
                break;
            case NodeType.Option:
                assertIs(Option, node);
                ctx.push(new LeafNode(node, ctx.current, optionName(node, 0)));
                break;
            case NodeType.Range:
                assertIs(Range, node);
                ctx.push(new LeafNode(node, ctx.current, node.lowerBound));
                break;
            case NodeType.Set:
                assertIs(GMLSet, node);
                ctx.push(new LeafNode(node, ctx.current, node.values[0]));
                break;
            case NodeType.Id:
                assertIs(Id, node);
                ctx.push(new LeafNode(node, ctx.current, node.defaultValue));
                break;
            case NodeType.VarName:
                assertIs(VarName, node);
                visit(resolve(node), ctx);
                break;
        }
        node.children.forEach((child) => visit(child, ctx));
        // exit
        switch (node.type) {
            case NodeType.Declaration:
            case NodeType.Field:
            case NodeType.Object:
            case NodeType.List:
            case NodeType.Map:
                ctx.pop();
        }
    };
    visit(file, ctx);
    return ctx.root;
};

///////////////////////////////////////////////////////////////////////////////
// UTIL
///////////////////////////////////////////////////////////////////////////////
class Stack<T> {
    constructor(private list: Array<T> = new Array<T>()) {}
    peek(): T | undefined {
        return this.list[this.list.length - 1];
    }
    push(val: T) {
        this.list.push(val);
    }
    pop(): T | undefined {
        return this.list.pop();
    }
    find(pred: (v: T) => boolean) {
        for (let i = this.list.length - 1; i >= 0; i--) {
            const val = this.list[i];
            if (pred(val)) return val;
        }
        return null;
    }
}

function assertIs<T extends ASTNode>(
    t: StaticIs<T>,
    o: ASTNode
): asserts o is T {
    if (!t.is(o)) throw Error(`Unexpected object type: ${o.type}`);
}

const size = (list: List): number => {
    const size = resolve(list.size);
    if (size !== null) {
        if (Range.is(size)) {
            return size.lowerBound;
        }
        if (typeof size === "number") {
            return size;
        }
        throw Error(`Unexpected size type: ${JSON.stringify(list.size)}`);
    }
    return 1;
};

const optionName = (option: Option, index: number): string =>
    `${(option.list.typeParam as VarName).identifier.str}#${index + 1}`;

///////////////////////////////////////////////////////////////////////////////
// OBJECT
///////////////////////////////////////////////////////////////////////////////
export function renderObject(file: File): Record<string, any> {
    const renderTree = generateRenderTree(file);
    const out: any[] = [];
    renderTree.write(out);
    // When writing an id write the name of the object/parent type and the index (keep track of how many have been written for each type)
    // Write out the tree but add placeholders for options
    // key the placeholders with paths (prop sequences)
    // expand the options to determine the amount of elements included in the option

    return out[0];
}

///////////////////////////////////////////////////////////////////////////////
// JSON
///////////////////////////////////////////////////////////////////////////////
export function renderJSON(file: File) {
    return JSON.stringify(renderObject(file), null, 4);
}
