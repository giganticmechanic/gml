import { Token } from "./token";
import { TokenType } from "./token-type";

export enum NodeType {
    File = "File",
    VarName = "VarName",
    Declaration = "Declaration",
    Field = "Field",
    Object = "Object",
    List = "List",
    Id = "Id",
    NumLiteral = "Numeric Literal",
    StrLiteral = "String Literal",
    BoolLiteral = "Boolean Literal",
    Number = "Number",
    String = "String",
    Bool = "Bool",
    Url = "Url",
    Image = "Image",
    Video = "Video",
    Sound = "Sound",
    Option = "Option",
    Range = "Range",
    Set = "Set",
    Map = "Map",
}

export type Primitive = Num | Str | Bool | Url | Video | Image | Sound;
export type Literal = NumLiteral | StrLiteral | BoolLiteral;
export type Type = Obj | Primitive | List | Range | GMLSet | GMLMap | Option;
export type Value = Type | Literal;

type Constructor = new (...args: any[]) => ASTNode;

const classToNode: Map<Constructor, NodeType> = new Map();

function registerType(type: NodeType, ctr: Constructor) {
    classToNode.set(ctr, type);
}

export abstract class ASTNode {
    protected constructor(
        readonly type: NodeType,
        readonly children: ASTNode[]
    ) {}
}

///////////////////////////////////////////////////////////////////////////////
// HELPER FUNCTIONS
///////////////////////////////////////////////////////////////////////////////
export const find = <T>(arr: T[], pred: (val: T) => boolean) => {
    for (let i = 0; i < arr.length; i++) {
        const val = arr[i];
        if (pred(arr[i])) return val;
    }
    return undefined;
};

// depth first traversal
export const traverse = (
    node: ASTNode,
    enter: (node: ASTNode) => void,
    exit: (node: ASTNode) => void = () => {}
) => {
    if (node.children.length === 0) return;
    const stack: { visited: boolean; node: ASTNode }[] = [
        { visited: false, node: node },
    ];
    while (stack.length > 0) {
        const entry = stack[0]!;
        if (entry.visited) {
            exit(entry.node);
            stack.shift();
        } else {
            entry.visited = true;
            enter(entry.node);
            const children = entry.node.children;
            for (let i = children.length - 1; i >= 0; i--) {
                stack.unshift({ visited: false, node: children[i] });
            }
        }
    }
};

// noinspection JSUnusedGlobalSymbols
export const hasChildren = (node: ASTNode) => {
    return node.children.length > 0;
};

// noinspection JSUnusedGlobalSymbols
export const getChildOfType = <T extends ASTNode>(
    node: ASTNode,
    cls: new (...args: any[]) => T
): T | null => {
    const type = classToNode.get(cls);
    if (type === null) return null;
    for (let i = 0; i < node.children.length; i++) {
        const n = node.children[i];
        if (n.type === type) return n as T;
    }
    return null;
};

// noinspection JSUnusedGlobalSymbols
export const getChildrenOfType = <T extends ASTNode>(
    node: ASTNode,
    cls: new (...args: any[]) => T
): T[] => {
    const c: T[] = [];
    const type = classToNode.get(cls);
    if (type === null) return c;

    for (let i = 0; i < node.children.length; i++) {
        const n = node.children[i];
        if (n.type === type) c.push(n as T);
    }
    return c;
};

export const getDescendantOfType = <T extends ASTNode>(
    node: ASTNode,
    cls: new (...args: any[]) => T
): T | null => {
    const type = classToNode.get(cls);
    if (type === null) return null;
    let stack: ASTNode[] = node.children.slice(0);
    while (stack.length > 0) {
        const n = stack.shift()!;
        if (n.type === type) return n as T;
        stack = stack.concat(n.children);
    }
    return null;
};

///////////////////////////////////////////////////////////////////////////////
// NODES
///////////////////////////////////////////////////////////////////////////////
interface Ctor<T> {
    new (...args: any[]): T;
}

export interface StaticIs<T> extends Ctor<T> {
    is(arg: any): arg is T;
}
function staticImplements<T>() {
    return (_constructor: T) => {};
}

@staticImplements<StaticIs<File>>()
export class File extends ASTNode {
    static is(arg: any): arg is File {
        return arg instanceof File;
    }
    constructor(readonly declarations: Declaration[]) {
        super(NodeType.File, declarations);
    }
}
registerType(NodeType.File, File);

@staticImplements<StaticIs<VarName>>()
export class VarName extends ASTNode {
    static is(arg: any): arg is VarName {
        return arg instanceof VarName;
    }

    private _value: Value | null = null;
    get value(): Value {
        if (this._value === null)
            throw Error(
                "Cannot access a variable's value until it's been resolved"
            );
        return this._value;
    }

    // noinspection JSUnusedLocalSymbols
    private setValue(value: Value) {
        this._value = value;
    }

    constructor(readonly identifier: Token) {
        super(NodeType.VarName, []);
    }
}

@staticImplements<StaticIs<Declaration>>()
export class Declaration extends ASTNode {
    static is(arg: any): arg is Declaration {
        return arg instanceof Declaration;
    }

    constructor(
        readonly identifier: Token,
        readonly value: Value | VarName,
        readonly comment: string | null
    ) {
        super(NodeType.Declaration, [value]);
        if (identifier.type !== TokenType.IDENTIFIER) {
            throw Error(`Expected an identifier but got: ${identifier.type}`);
        }
    }
}
registerType(NodeType.Declaration, Declaration);

@staticImplements<StaticIs<Obj>>()
export class Obj extends ASTNode {
    static is(arg: any): arg is Obj {
        return arg instanceof Obj;
    }

    constructor(readonly fields: Field[]) {
        super(NodeType.Object, fields);
    }
}
registerType(NodeType.Object, Obj);

@staticImplements<StaticIs<Field>>()
export class Field extends ASTNode {
    static is(arg: any): arg is Field {
        return arg instanceof Field;
    }

    get isId() {
        return Id.is(this.value);
    }

    constructor(
        readonly identifier: Token,
        readonly value: Value | Id | VarName,
        readonly comment: string | null,
        readonly isOptional: boolean
    ) {
        super(NodeType.Field, [value]);
        if (identifier.type !== TokenType.IDENTIFIER) {
            throw Error(`Expected an identifier but got: ${identifier.type}`);
        }
    }
}
registerType(NodeType.Field, Field);

@staticImplements<StaticIs<List>>()
export class List extends ASTNode {
    static is(arg: any): arg is List {
        return arg instanceof List;
    }

    constructor(
        readonly typeParam: Type | VarName,
        readonly size: number | Range | VarName | null
    ) {
        super(NodeType.List, []);
    }
}
registerType(NodeType.List, List);

export abstract class LiteralValue<
    T extends number | string | boolean
> extends ASTNode {
    protected constructor(readonly value: T, type: NodeType) {
        super(type, []);
    }
}

@staticImplements<StaticIs<NumLiteral>>()
export class NumLiteral extends LiteralValue<number> {
    static is(arg: any): arg is NumLiteral {
        return arg instanceof NumLiteral;
    }
    constructor(value: number) {
        super(value, NodeType.NumLiteral);
    }
}
registerType(NodeType.NumLiteral, NumLiteral);

@staticImplements<StaticIs<StrLiteral>>()
export class StrLiteral extends LiteralValue<string> {
    static is(arg: any): arg is StrLiteral {
        return arg instanceof StrLiteral;
    }
    constructor(value: string) {
        super(value, NodeType.StrLiteral);
    }
}
registerType(NodeType.StrLiteral, StrLiteral);

@staticImplements<StaticIs<BoolLiteral>>()
export class BoolLiteral extends LiteralValue<boolean> {
    static is(arg: any): arg is BoolLiteral {
        return arg instanceof BoolLiteral;
    }
    constructor(value: boolean) {
        super(value, NodeType.BoolLiteral);
    }
}
registerType(NodeType.BoolLiteral, BoolLiteral);

export abstract class PrimitiveValue<
    T extends number | string | boolean
> extends ASTNode {
    protected constructor(readonly defaultValue: T, type: NodeType) {
        super(type, []);
    }
}

@staticImplements<StaticIs<Id>>()
export class Id extends ASTNode {
    static is(arg: any): arg is Id {
        return arg instanceof Id;
    }

    constructor(readonly defaultValue: string | null | VarName) {
        super(NodeType.Id, []);
    }
}
registerType(NodeType.Id, Id);

@staticImplements<StaticIs<Num>>()
export class Num extends PrimitiveValue<number> {
    static is(arg: any): arg is Num {
        return arg instanceof Num;
    }

    constructor(readonly defaultValue: number) {
        super(defaultValue, NodeType.Number);
    }
}
registerType(NodeType.Number, Num);

@staticImplements<StaticIs<Str>>()
export class Str extends PrimitiveValue<string> {
    static is(arg: any): arg is Str {
        return arg instanceof Str;
    }

    constructor(readonly defaultValue: string) {
        super(defaultValue, NodeType.String);
    }
}
registerType(NodeType.String, Str);

@staticImplements<StaticIs<Bool>>()
export class Bool extends PrimitiveValue<boolean> {
    static is(arg: any): arg is Bool {
        return arg instanceof Bool;
    }

    constructor(readonly defaultValue: boolean) {
        super(defaultValue, NodeType.Bool);
    }
}
registerType(NodeType.Bool, Bool);

@staticImplements<StaticIs<Url>>()
export class Url extends PrimitiveValue<string> {
    static is(arg: any): arg is Url {
        return arg instanceof Url;
    }

    constructor(readonly defaultValue: string) {
        super(defaultValue, NodeType.Url);
    }
}
registerType(NodeType.Url, Url);

@staticImplements<StaticIs<Image>>()
export class Image extends PrimitiveValue<string> {
    static is(arg: any): arg is Image {
        return arg instanceof Image;
    }

    constructor(readonly defaultValue: string) {
        super(defaultValue, NodeType.Image);
    }
}
registerType(NodeType.Image, Image);

@staticImplements<StaticIs<Video>>()
export class Video extends PrimitiveValue<string> {
    static is(arg: any): arg is Video {
        return arg instanceof Video;
    }

    constructor(readonly defaultValue: string) {
        super(defaultValue, NodeType.Video);
    }
}
registerType(NodeType.Video, Video);

@staticImplements<StaticIs<Sound>>()
export class Sound extends PrimitiveValue<string> {
    static is(arg: any): arg is Sound {
        return arg instanceof Sound;
    }

    constructor(readonly defaultValue: string) {
        super(defaultValue, NodeType.Sound);
    }
}
registerType(NodeType.Sound, Sound);

@staticImplements<StaticIs<Option>>()
export class Option extends ASTNode {
    static is(arg: any): arg is Option {
        return arg instanceof Option;
    }

    private _object: Obj | null = null;
    private _list: List | null = null;

    // The list that terminated the path
    get list(): List {
        if (this._list === null) {
            throw Error("Can't get an option's list before resolving it");
        }
        return this._list;
    }

    // The object type that the type parameter of the list resolves to
    get object(): Obj {
        if (this._object === null) {
            throw Error("Can't get an option's object before resolving it");
        }
        return this._object;
    }

    constructor(readonly path: Token[]) {
        super(NodeType.Option, []);
    }
}
registerType(NodeType.Option, Option);

@staticImplements<StaticIs<Range>>()
export class Range extends ASTNode {
    static is(arg: any): arg is Range {
        return arg instanceof Range;
    }

    constructor(readonly lowerBound: number, readonly upperBound: number) {
        super(NodeType.Range, []);
    }
}
registerType(NodeType.Range, Range);

@staticImplements<StaticIs<GMLSet>>()
export class GMLSet extends ASTNode {
    static is(arg: any): arg is GMLSet {
        return arg instanceof GMLSet;
    }

    constructor(readonly values: string[] | number[]) {
        super(NodeType.Set, []);
    }
}
registerType(NodeType.Set, GMLSet);

@staticImplements<StaticIs<GMLMap>>()
export class GMLMap extends ASTNode {
    static is(arg: any): arg is GMLMap {
        return arg instanceof GMLMap;
    }

    constructor(
        readonly keyType: Str | Option,
        readonly valueType: Type | VarName
    ) {
        super(NodeType.Map, []);
    }
}
registerType(NodeType.Map, GMLMap);
