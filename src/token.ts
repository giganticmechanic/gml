import { TokenType } from "./token-type";

export interface Token {
    str: string;
    type: TokenType;
    offset: number;
}

export function printToken(token: Token): string {
    return `[${token.type}]:${token.str}`;
}

export function getNumValue(token: Token) {
    if (token.type !== TokenType.NUM) {
        throw Error("Cannot get number value from non-number token");
    }
    const val = parseFloat(token.str);
    if (val === undefined)
        throw Error(`Could not parse ${token.str} as a number`);
    return val;
}

export function getBoolValue(token: Token) {
    if (token.type !== TokenType.BOOL) {
        throw Error("Cannot get bool value from non-bool token");
    }
    switch (token.str) {
        case "true":
            return true;
        case "false":
            return false;
        default:
            throw Error(`Bool token has invalid string: ${token.str}`);
    }
}
