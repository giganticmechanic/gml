import { parse } from "./parser";
import {
    Bool,
    BoolLiteral,
    Declaration,
    getDescendantOfType,
    GMLMap,
    GMLSet,
    List,
    NodeType,
    Num,
    NumLiteral,
    Obj,
    Option,
    Range,
    StaticIs,
    Str,
    StrLiteral,
    Url,
    VarName,
} from "./ast-node";
import { TokenType } from "./token-type";
import { lex } from "./lexer";
import "./util/jest-helpers";
import { clearErrors, getErrors } from "./error";

function assertIs<T>(is: StaticIs<T>, obj: any): asserts obj is T {
    if (!is.is(obj)) throw Error(`Expected ${is.name} but got ${obj.type}`);
}

describe("Parser", () => {
    afterEach(clearErrors);
    describe("Declaration", () => {
        it("Attaches comments", () => {
            const file = parse(lex("<<Comment>>a=1"));
            expect(getErrors()).toHaveLength(0);
            expect(file.declarations[0].comment).toEqual("Comment");
        });
        describe("Variables", () => {
            it("Errors when assignment isn't followed by a value", () => {
                parse(lex("derp ="));
                expect(getErrors()).toHaveLength(1);
            });
            describe("Resolution", () => {
                it("Can resolve value variables", () => {
                    const tokens = lex("a=1 b=a");
                    const file = parse(tokens);
                    expect(getErrors()).toHaveLength(0);
                    const aDecl = file.declarations[0];
                    const bDecl = file.declarations[1];
                    assertIs(VarName, bDecl.value);
                    assertIs(NumLiteral, bDecl.value.value);
                    expect(bDecl.value.value).toEqual(aDecl.value);
                });
                it("Can resolve type variables", () => {
                    const tokens = lex(`a=list(string("default")) b=a`);
                    const file = parse(tokens);
                    expect(getErrors()).toHaveLength(0);
                    const aDecl = file.declarations[0];
                    const bDecl = file.declarations[1];
                    assertIs(VarName, bDecl.value);
                    assertIs(List, bDecl.value.value);
                    expect(bDecl.value.value).toEqual(aDecl.value);
                });
                it("Can resolve literals in defaults", () => {
                    const input = `nl = 1.5 n = number(nl) bl = true b = bool(bl) sl = "string" s = string(sl)`;
                    const tokens = lex(input);
                    const file = parse(tokens);
                    expect(getErrors()).toHaveLength(0);
                    const n = file.declarations[1];
                    assertIs(Num, n.value);
                    expect(n.value.defaultValue).toEqual(1.5);
                    const b = file.declarations[3];
                    expect((b.value as Bool).defaultValue).toEqual(true);
                    const s = file.declarations[5];
                    expect((s.value as Str).defaultValue).toEqual("string");
                });
            });
            describe("Literals", () => {
                it("Parses integer literal declaration", () => {
                    const file = parse(lex("three = 3"));
                    const three = getDescendantOfType(file, NumLiteral)!;
                    expect(three).not.toBeNull();
                    expect(three.value).toEqual(3);
                });
                it("Parses float literal declaration", () => {
                    const file = parse(lex("half = 0.5"));
                    const half = getDescendantOfType(file, NumLiteral)!;
                    expect(half).not.toBeNull();
                    expect(half.value).toEqual(0.5);
                });
                it("Parses string literal declaration", () => {
                    const file = parse(lex('word = "to the herd"'));
                    const word = getDescendantOfType(file, StrLiteral)!;
                    expect(word).not.toBeNull();
                    expect(word.value).toEqual("to the herd");
                });
                it("Parses 'true' literal declaration", () => {
                    const file = parse(lex("fact = true"));
                    const fact = getDescendantOfType(file, BoolLiteral)!;
                    expect(fact).not.toBeNull();
                    expect(fact.value).toEqual(true);
                });
                it("Parses 'false' literal declaration", () => {
                    const file = parse(lex("lie = false"));
                    const fact = getDescendantOfType(file, BoolLiteral)!;
                    expect(fact).not.toBeNull();
                    expect(fact.value).toEqual(false);
                });
            });
        });
        describe("Primitives", () => {
            // TODO: test identifier default values for all of these
            it("Errors on numbers without defaults", () => {
                const file = parse(lex("n = number"));
                const n = getDescendantOfType(file, Num)!;
                expect(getErrors()).toHaveLength(1);
            });
            it("Parses numbers with defaults", () => {
                const file = parse(lex("n = number(1)"));
                const n = getDescendantOfType(file, Num)!;
                expect(getErrors()).toHaveLength(0);
                expect(n).not.toBeNull();
                expect(n.type).toEqual(NodeType.Number);
                expect(n.defaultValue).toEqual(1);
            });
            it("Errors on strings without defaults", () => {
                const file = parse(lex("s = string"));
                const s = getDescendantOfType(file, Str)!;
                expect(getErrors()).toHaveLength(1);
            });
            it("Parses strings with defaults", () => {
                const file = parse(lex('s = string("Hello")'));
                const s = getDescendantOfType(file, Str)!;
                expect(getErrors()).toHaveLength(0);
                expect(s).not.toBeNull();
                expect(s.type).toEqual(NodeType.String);
                expect(s.defaultValue).toEqual("Hello");
            });
            it("Errors on bools without defaults", () => {
                const file = parse(lex("b = bool"));
                const b = getDescendantOfType(file, Bool)!;
                expect(getErrors()).toHaveLength(1);
            });
            it("Parses bools with defaults", () => {
                const file = parse(lex("b = bool(true)"));
                const b = getDescendantOfType(file, Bool)!;
                expect(getErrors()).toHaveLength(0);
                expect(b).not.toBeNull();
                expect(b.defaultValue).toEqual(true);
            });
            it("Errors on urls without defaults", () => {
                const file = parse(lex("u = url"));
                const u = getDescendantOfType(file, Url)!;
                expect(getErrors()).toHaveLength(1);
            });
            it("Parses urls with defaults", () => {
                const file = parse(lex('u = url("kermit://hearts.piggy")'));
                const u = getDescendantOfType(file, Url)!;
                expect(getErrors()).toHaveLength(0);
            });
        });
        describe("Lists", () => {
            it("Accepts variables for the type", () => {
                const input = `t=string("default") l=list(t)`;
                const tokens = lex(input);
                const file = parse(tokens);
                expect(getErrors()).toHaveLength(0);
                const l = getDescendantOfType(file, List)!;
                expect(l).not.toBeNull();
                expect(l.typeParam.type).toEqual(NodeType.VarName);
            });
            it("Accepts ranges for the size", () => {
                const input = `l=list(string("default") [1 2])`;
                const tokens = lex(input);
                const file = parse(tokens);
                expect(getErrors()).toHaveLength(0);
                const l = getDescendantOfType(file, List)!;
                expect(l).not.toBeNull();
                expect(Range.is(l.size)).toEqual(true);
            });
            it("Accepts variables for the size", () => {
                const input = `a=99 l=list(string("default") a)`;
                const tokens = lex(input);
                const file = parse(tokens);
                expect(getErrors()).toHaveLength(0);
                const l = getDescendantOfType(file, List)!;
                expect(l).not.toBeNull();
                assertIs(VarName, l.size);
                assertIs(NumLiteral, l.size.value);
                expect(l.size.value.value).toEqual(99);
            });
            it("Parses number lists", () => {
                const file = parse(lex("l = list(number(1))"));
                const list = getDescendantOfType(file, List)!;
                expect(getErrors()).toHaveLength(0);
                expect(list).not.toBeNull();
                expect(list.typeParam.type).toEqual(NodeType.Number);
                expect(list.size).toEqual(null);
            });
            it("Parses number lists with size", () => {
                const file = parse(lex("l = list(number(2) 2)"));
                const list = getDescendantOfType(file, List)!;
                expect(getErrors()).toHaveLength(0);
                expect(list).not.toBeNull();
                expect(list.typeParam.type).toEqual(NodeType.Number);
                expect(list.size).toEqual(2);
            });
            it("Parses number lists with default and size", () => {
                const file = parse(lex("l = list(number(4) 2)"));
                const list = getDescendantOfType(file, List)!;
                expect(getErrors()).toHaveLength(0);
                expect(list).not.toBeNull();
                expect(list.typeParam.type).toEqual(NodeType.Number);
                expect((list.typeParam as Num).defaultValue).toEqual(4);
                expect(list.size).toEqual(2);
            });
            it("Parses string lists", () => {
                const file = parse(lex(`l = list(string("default"))`));
                const list = getDescendantOfType(file, List)!;
                expect(getErrors()).toHaveLength(0);
                expect(list).not.toBeNull();
                expect(list.typeParam.type).toEqual(NodeType.String);
                expect(list.size).toEqual(null);
            });
            it("Parses string lists with size", () => {
                const file = parse(lex(`l = list(string("default") 2)`));
                const list = getDescendantOfType(file, List)!;
                expect(getErrors()).toHaveLength(0);
                expect(list).not.toBeNull();
                expect(list.typeParam.type).toEqual(NodeType.String);
                expect(list.size).toEqual(2);
            });
            it("Parses bool lists", () => {
                const file = parse(lex("l = list(bool(true))"));
                const list = getDescendantOfType(file, List)!;
                expect(getErrors()).toHaveLength(0);
                expect(list).not.toBeNull();
                expect(list.typeParam.type).toEqual(NodeType.Bool);
                expect(list.size).toEqual(null);
            });
            it("Parses bool lists with size", () => {
                const file = parse(lex("l = list(bool(true) 2)"));
                const list = getDescendantOfType(file, List)!;
                expect(getErrors()).toHaveLength(0);
                expect(list).not.toBeNull();
                expect(list.typeParam.type).toEqual(NodeType.Bool);
                expect(list.size).toEqual(2);
            });
            it("Parses url lists", () => {
                const file = parse(
                    lex(`l = list(url("kermit://hearts.piggy"))`)
                );
                const list = getDescendantOfType(file, List)!;
                expect(getErrors()).toHaveLength(0);
                expect(list).not.toBeNull();
                expect(list.typeParam.type).toEqual(NodeType.Url);
                expect(list.size).toEqual(null);
            });
            it("Parses url lists with size", () => {
                const file = parse(
                    lex(`l = list(url("kermit://hearts.piggy") 2)`)
                );
                const list = getDescendantOfType(file, List)!;
                expect(getErrors()).toHaveLength(0);
                expect(list).not.toBeNull();
                expect(list.typeParam.type).toEqual(NodeType.Url);
                expect(list.size).toEqual(2);
            });
            it("Parses nested lists", () => {
                const file = parse(lex("l = list(list(number(3)))"));
                const list = getDescendantOfType(file, List)!;
                expect(getErrors()).toHaveLength(0);
                expect(list).not.toBeNull();
                expect(list.typeParam.type).toEqual(NodeType.List);
            });
        });
        describe("Ranges", () => {
            it("Parses ranges", () => {
                const file = parse(lex("a=[1 3]"));
                expect(getErrors()).toHaveLength(0);
                const range = file.declarations[0].value;
                if (Range.is(range)) {
                    expect(range.lowerBound).toEqual(1);
                    expect(range.upperBound).toEqual(3);
                } else {
                    fail("Expected range to be a range");
                }
            });
            it("Errors if missing closing bracket", () => {
                parse(lex("a=[1 3"));
                expect(getErrors()).toHaveLength(1);
            });
            it("Errors if bounds are not numbers", () => {
                parse(lex(`a=["1" 3]`));
                expect(getErrors()).toHaveLength(1);
                parse(lex(`a=[1 "3"]`));
                expect(getErrors()).toHaveLength(2);
            });
        });
        describe("Sets", () => {
            it("Parses numeric sets", () => {
                const file = parse(lex("a=set(1 2 3)"));
                expect(getErrors()).toHaveLength(0);
                const set = file.declarations[0].value;
                if (GMLSet.is(set)) {
                    expect(set.values).toEqual([1, 2, 3]);
                } else {
                    fail("Expected set to be a set");
                }
            });
            it("Parses string sets", () => {
                const file = parse(lex('a=set("A" "B")'));
                expect(getErrors()).toHaveLength(0);
                const set = file.declarations[0].value;
                if (GMLSet.is(set)) {
                    expect(set.values).toEqual(["A", "B"]);
                } else {
                    fail("Expected set to be a set");
                }
            });
            it("Errors if missing closing bracket", () => {
                parse(lex("a=set(1 2"));
                expect(getErrors()).toHaveLength(1);
            });
            it("Errors if mixed value types", () => {
                parse(lex('a=set(1 "A")'));
                expect(getErrors()).toHaveLength(1);
            });
            it("Errors if empty set", () => {
                parse(lex("a=set()"));
                expect(getErrors()).toHaveLength(1);
            });
        });
        describe("Maps", () => {
            it("Parses maps with reference keys", () => {
                const tokens = lex(
                    `b={name:id("id")} a={name:id("id2") l:list(b 3)} m=map(option(a.l) number(3))`
                );
                const file = parse(tokens);
                expect(getErrors()).toHaveLength(0);
                const m = getDescendantOfType(file, GMLMap)!;
                expect(m).not.toBeNull();
                expect(m.keyType.type).toEqual(NodeType.Option);
                expect(m.valueType.type).toEqual(NodeType.Number);
            });
            it("Parses maps with string keys", () => {
                const file = parse(lex(`m=map(string("key") number(3))`));
                expect(getErrors()).toHaveLength(0);
                const m = getDescendantOfType(file, GMLMap)!;
                expect(m).not.toBeNull();
                expect(m.keyType.type).toEqual(NodeType.String);
                expect(m.valueType.type).toEqual(NodeType.Number);
            });
            it("Errors if key is not string or ref", () => {
                parse(lex("m=map(1 number)"));
                expect(getErrors()).toHaveLength(1);
            });
            it("Errors if not closed with parenthesis", () => {
                parse(lex("m=map(string number"));
                expect(getErrors()).toHaveLength(1);
            });
        });
        describe("Identifiers", () => {
            it("Errors when using bool as name", () => {
                parse(lex("bool = true"));
                expect(getErrors()).toHaveLength(1);
            });
            it("Errors when using true as name", () => {
                parse(lex("true = true"));
                expect(getErrors()).toHaveLength(1);
            });
            it("Errors when using false as name", () => {
                parse(lex("false = false"));
                expect(getErrors()).toHaveLength(1);
            });
            it("Errors when using number as name", () => {
                parse(lex("number = 1"));
                expect(getErrors()).toHaveLength(1);
            });
            it("Errors when using string as name", () => {
                parse(lex('string = "word"'));
                expect(getErrors()).toHaveLength(1);
            });
            it("Errors when using url as name", () => {
                parse(lex('url = "http://derp.biz"'));
                expect(getErrors()).toHaveLength(1);
            });
        });
        describe("Objects", () => {
            it("Recognizes identifier fields", () => {
                const file = parse(lex(`o={x:id("id")}`));
                expect(getErrors()).toHaveLength(0);
                const o = getDescendantOfType(file, Obj)!;
                expect(o.fields[0].isId).toEqual(true);
            });
            it("Errors if more than one identifier field", () => {
                parse(lex(`o={a:id b:id("id")}`));
                expect(getErrors()).toHaveLength(1);
            });
            it("Attaches comments to fields", () => {
                const file = parse(lex("o={<<Comment>>x:number(33)}"));
                expect(getErrors()).toHaveLength(0);
                const o = getDescendantOfType(file, Obj)!;
                expect(o.fields[0].comment).toEqual("Comment");
            });
            it("Parses empty objects", () => {
                const file = parse(lex("o = {}"));
                const obj = getDescendantOfType(file, Obj)!;
                expect(getErrors()).toHaveLength(0);
                expect(obj).not.toBeNull();
                expect(obj.fields).toHaveLength(0);
            });
            it("Parses objects", () => {
                const file = parse(lex("o={a:1}"));
                const obj = getDescendantOfType(file, Obj)!;
                expect(getErrors()).toHaveLength(0);
                expect(obj).not.toBeNull();
                const num = getDescendantOfType(obj, NumLiteral)!;
                expect(num).not.toBeNull();
                expect(num.value).toEqual(1);
            });
            it("Parses nested objects", () => {
                const file = parse(lex("o={a:{}}"));
                const obj = getDescendantOfType(file, Obj)!;
                expect(obj).not.toBeNull();
                const child = getDescendantOfType(obj, Obj)!;
                expect(child).not.toBeNull();
            });
            it("Errors if no closing brace", () => {
                const tokens = lex("o = {x:{}");
                parse(tokens);
                expect(getErrors()).toHaveLength(1);
            });
        });
        describe("Options", () => {
            it("Can parse options", () => {
                const input = `a={n:id("1")} b={n:id("2") l:list(a 3)} c={r:option(b.l)}`;
                const tokens = lex(input);
                const file = parse(tokens);
                expect(getErrors()).toHaveLength(0);
                const ref = getDescendantOfType(file, Option)!;
                expect(ref.path[0].str).toEqual("b");
                expect(ref).not.toBeNull();
            });
            it("Can parse multi element paths", () => {
                const input = `Proposal={name:id("1")} Scenario={name:id("2") data:{proposals:list(Proposal 9)} sampleProposal:option(Scenario.data.proposals)}`;

                const tokens = lex(input);
                const file = parse(tokens);
                expect(getErrors()).toHaveLength(0);
                const ref = getDescendantOfType(file, Option)!;
                expect(ref.path[0].str).toEqual("Scenario");
                expect(ref.path[2].str).toEqual("proposals");
                expect(ref).not.toBeNull();
            });
            it("Does not parse options to literals", () => {
                const input = "a={n:id l:1} b=option(a.l)";
                const tokens = lex(input);
                parse(tokens);
                expect(getErrors()).toHaveLength(1);
            });
            it("Does not parse options to primitives", () => {
                const input = "a={n:id l:string} b=option(a.l)";
                const tokens = lex(input);
                parse(tokens);
                expect(getErrors()).toHaveLength(1);
            });
            it("Does not allow option paths to start with objects that do not have an identifier field", () => {
                const input = "a={b:1} c=option(a)";
                const tokens = lex(input);
                parse(tokens);
                expect(getErrors()).toHaveLength(1);
            });
        });
    });
    describe("Exceptions", () => {
        it("Throws on empty input", () => {
            expect(() => parse([])).toThrow();
        });
        it("Throws if tokens aren't terminated with an EOF", () => {
            expect(() =>
                parse([{ type: TokenType.OPEN_BRACKET, offset: 0, str: "{" }])
            ).toThrow();
        });
    });
});
