import { renderObject } from "./renderObject";
import * as fs from "fs";
import * as path from "path";
import { lex } from "./lexer";
import { parse } from "./parser";
import { renderMST } from "./renderMST";

const input = fs
    .readFileSync(path.join(__dirname, "../assets/test.gml"))
    .toString();

describe("Renders", () => {
    describe("MST", () => {
        it("Check output", () => {
            console.log(renderMST(parse(lex(input))));
        });
    });
    describe("Object", () => {
        const out = renderObject(parse(lex(input)));
        it("Renders issue", () => {
            const issue = out["Issue"];
            expect(typeof issue["name"]).toEqual("string");
            expect(typeof issue["description"]).toEqual("string");
            expect(typeof issue["image"]).toEqual("string");
            expect(Array.isArray(issue["proposals"])).toEqual(true);
            expect(issue["proposals"].length).toEqual(3);
        });

        it("Check output", () => {
            console.log(JSON.stringify(out["CoreScenario"], null, 4));
        });
    });
});
