import program from "commander";
import { readFileSync, existsSync, readdirSync, writeFileSync } from "fs";
import * as path from "path";
import { resolve, extname } from "path";
import { lex, parse } from "./index";
import { renderMST } from "./renderMST";
import { renderJSON, renderObject } from "./renderObject";
import { merge } from "lodash";

program
    .version("1.0.0")
    // Input
    .option("-i --input <path>", "Input gml file")

    // Output
    .option("-o --output", "Output path")
    .option("-j --json", "Generate JSON output")
    .option("-m --mst", "Generate mobx-state-tree TypeScript output")
    // Test
    // .option("-t --test <path>", "JSON file you want to test for validity")
    .parse(process.argv);

let { input, json, mst, output } = program;
console.log(input, json, mst);

process.on("uncaughtException", (e) => {
    if (e.name === "Exit") {
        console.log(e.message);
        process.exitCode = 1;
    }
    process.exitCode = 0;
});

const quitWithMessage = (message: string) => {
    const e = new Error(message);
    e.name = "Exit";
    throw e;
};

if (!output) {
    console.log(
        "No output location provided. Assuming you want the files in this directory."
    );
    output = process.cwd();
}

let inputPath: string;
if (input) {
    inputPath = resolve(program.input);
} else {
    console.log("No input file specified. Looking for one in this folder...");
    const files = readdirSync(process.cwd());
    const filesWithGMLExt = files.filter((file) => extname(file) === ".gml");

    if (filesWithGMLExt.length === 0) {
        quitWithMessage(
            "No gml file was specified and there are no gml files in the folder."
        );
    }

    if (filesWithGMLExt.length > 1) {
        quitWithMessage(
            "No gml file was specified and there are multiple gml files in the folder so I don't know which to pick."
        );
    }

    inputPath = resolve(filesWithGMLExt[0]);
}

console.log("Using gml file:", inputPath);

if (!(json || mst)) {
    console.log(
        "You provided no output options. Assuming you want JSON output"
    );
    json = true;
}

const fileString = readFileSync(inputPath).toString();
const ast = parse(lex(fileString));

if (mst) {
    const out = path.join(
        resolve(output),
        path.basename(inputPath, path.extname(inputPath)) + ".ts"
    );
    writeFileSync(out, renderMST(ast));
    console.log("Output TypeScript file:", out);
}

if (json) {
    const out = path.join(
        resolve(output),
        path.basename(inputPath, path.extname(inputPath)) + ".json"
    );
    if (existsSync(out)) {
        console.log("Found existing json file named: " + output);
        const left = renderObject(ast).CoreScenario;
        const right = JSON.parse(readFileSync(out).toString());
        const merged = merge(left, right);
        writeFileSync(out, JSON.stringify(merged));
    } else {
        const jsonOutput = renderJSON(ast);
        writeFileSync(out, jsonOutput);
    }
    console.log("Output JSON file:", out);
}
