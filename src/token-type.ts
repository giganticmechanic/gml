import { Token } from "./token";

export enum TokenType {
    OPEN_PAREN = "Open Paren",
    CLOSED_PAREN = "Closed Paren",
    OPEN_BRACE = "Open Brace",
    CLOSED_BRACE = "Closed Brace",
    OPEN_BRACKET = "Open Bracket",
    CLOSED_BRACKET = "Closed Bracket",
    OPEN_ANGLE_BRACKET = "Open Angle Bracket",
    CLOSED_ANGLE_BRACKET = "Closed Angle Bracket",
    QUESTION = "Question",
    STAR = "Star",
    COMMA = "Comma",
    PERIOD = "Period",
    EQUALS = "Equals",
    COLON = "Colon",
    STR = "String",
    NUM = "Number",
    BOOL = "Bool",
    ID = "Id",
    IDENTIFIER = "Identifier",
    COMMENT = "Comment",
    EOF = "EOF",
}

const isTokenType = (tokenType: TokenType) => (token: Token) =>
    token.type === tokenType;

export const isEOF = isTokenType(TokenType.EOF);
export const isIdentifier = isTokenType(TokenType.IDENTIFIER);
export const isEquals = isTokenType(TokenType.EQUALS);
export const isColon = isTokenType(TokenType.COLON);
export const isStar = isTokenType(TokenType.STAR);
export const isComma = isTokenType(TokenType.COMMA);
export const isPeriod = isTokenType(TokenType.PERIOD);
export const isComment = isTokenType(TokenType.COMMENT);
export const isNumLiteral = isTokenType(TokenType.NUM);
export const isStrLiteral = isTokenType(TokenType.STR);
export const isBoolLiteral = isTokenType(TokenType.BOOL);
export const isOpenBrace = isTokenType(TokenType.OPEN_BRACE);
export const isClosedBrace = isTokenType(TokenType.CLOSED_BRACE);
export const isOpenAngleBracket = isTokenType(TokenType.OPEN_ANGLE_BRACKET);
export const isClosedAngleBracket = isTokenType(TokenType.CLOSED_ANGLE_BRACKET);
export const isOpenBracket = isTokenType(TokenType.OPEN_BRACKET);
export const isClosedBracket = isTokenType(TokenType.CLOSED_BRACKET);
export const isOpenParen = isTokenType(TokenType.OPEN_PAREN);
export const isClosedParen = isTokenType(TokenType.CLOSED_PAREN);
export const isQuestion = isTokenType(TokenType.QUESTION);
export const isId = isTokenType(TokenType.ID);
