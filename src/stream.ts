interface Data<T> {
    length: number;
    [position: number]: T;
}

type Pred<T> = (value: T) => boolean;

export interface Stream<T> {
    readonly data: Data<T>;
    current: number;
}

export function createStream<T>(data: Data<T>) {
    return { data: data, current: 0 };
}

export const isFinished = <T>(stream: Stream<T>) => {
    return stream.current >= stream.data.length;
};

export const advance = <T>(stream: Stream<T>): T => {
    if (isFinished(stream)) throw "Tried to advance past end of stream.";
    return stream.data[stream.current++];
};

export const peek = <T>(stream: Stream<T>, offset: number = 0): T => {
    const d = stream.data[stream.current + offset];
    if (d === undefined) throw Error("Peeked out of bounds");
    return d;
};

export const test = <T>(stream: Stream<T>, predicate: Pred<T>) => {
    if (isFinished(stream)) return false;
    const v = peek(stream);
    if (v === undefined) return false;
    return predicate(v);
};

export const testAhead = <T>(
    stream: Stream<T>,
    offset: number,
    predicate: Pred<T>
) => {
    if (stream.current + offset >= stream.data.length) return false;
    const v = peek(stream, offset);
    if (v === undefined) return false;
    return predicate(v);
};

export const advanceIf = <T>(
    stream: Stream<T>,
    predicate: Pred<T>,
    errHandler?: (unexpectedValue: T) => void
): T => {
    if (isFinished(stream)) throw Error("Unexpectedly reached end of stream.");
    if (!test(stream, predicate)) {
        const t = peek(stream);
        if (errHandler !== undefined) errHandler(t);
        return t;
    }
    return advance(stream);
};
