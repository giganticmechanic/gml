import { lex } from "./lexer";
import { clearErrors, getErrorInfo, getErrors } from "./error";
import { parse } from "./parser";

describe("Errors", () => {
    afterEach(clearErrors);
    describe("Context Info", () => {
        it("Returns one info per error", () => {
            const input = "\na=1\nb=&\n|";
            lex(input);
            const errors = getErrors();
            expect(errors).toHaveLength(2);
            const infos = getErrorInfo(errors, input);
            expect(infos).toHaveLength(errors.length);
        });
        it("Reports correct line", () => {
            const input = "\na=1\nb=&";
            lex(input);
            const errors = getErrors();
            expect(errors).toHaveLength(1);
            const infos = getErrorInfo(errors, input);
            expect(infos[0].line).toEqual(3);
        });
        it("Reports correct column", () => {
            const input = "\na=1\nb=&";
            lex(input);
            const errors = getErrors();
            expect(errors).toHaveLength(1);
            const infos = getErrorInfo(errors, input);
            expect(infos[0].column).toEqual(3);
        });
        it("Handles parser errors", () => {
            /*
            const input = "\na=1\nb=\nc=3";
            console.log(lex(input));
            parse(lex(input));
            const errors = getErrors();
            expect(errors).toHaveLength(1);
            const infos = getErrorInfo(errors, input);
            console.log(infos);
            expect(infos[0].column).toEqual(3);
            */
        });
    });
});
