import { getBoolValue, getNumValue, printToken, Token } from "./token";
import {
    advance,
    advanceIf,
    createStream,
    isFinished,
    peek,
    Stream,
    test,
} from "./stream";
import { getErrors, ParseError, pushError, ResolutionError } from "./error";
import {
    isBoolLiteral,
    isClosedBrace,
    isClosedBracket,
    isClosedParen,
    isColon,
    isComment,
    isEOF,
    isEquals,
    isId,
    isIdentifier,
    isNumLiteral,
    isOpenBrace,
    isOpenBracket,
    isOpenParen,
    isPeriod,
    isQuestion,
    isStrLiteral,
    TokenType,
} from "./token-type";
import { isReserved, Reserved } from "./reserved";
import {
    ASTNode,
    Bool,
    BoolLiteral,
    Declaration,
    Field,
    File,
    GMLMap,
    GMLSet,
    Id,
    Image,
    List,
    Num,
    NumLiteral,
    Obj,
    Range,
    Option,
    Sound,
    Str,
    StrLiteral,
    Type,
    Url,
    Value,
    Video,
    LiteralValue,
    VarName,
    PrimitiveValue,
} from "./ast-node";

///////////////////////////////////////////////////////////////////////////////
// HELPER FUNCTIONS
///////////////////////////////////////////////////////////////////////////////
const error = (message: string, token: Token) => {
    throw new ParseError(message, token);
};

const expected = (expectedString: string) => (t: Token) => {
    const errMsg = `Expected: ( ${expectedString} ) Received: ( ${t.str} )`;
    error(errMsg, t);
};

const isDone = (stream: Stream<Token>) => {
    return isFinished(stream) || isEOF(peek(stream));
};

///////////////////////////////////////////////////////////////////////////////
// PARSING
///////////////////////////////////////////////////////////////////////////////
type ParseContext = {
    stream: Stream<Token>;
    declarations: Declaration[];
    // TODO: Get rid of everything except vars
    vars: VarName[];
    options: Option[];
};

type ParseFunc<T extends ASTNode> = (context: ParseContext) => T;

export function parse(tokens: Token[]): File {
    if (tokens.length === 0) throw Error("Cannot parse empty token list.");
    if (tokens[tokens.length - 1].type !== TokenType.EOF)
        throw Error("Token list must end with EOF token.");

    // Parse State
    const context: ParseContext = {
        stream: createStream(tokens),
        declarations: [],
        vars: [],
        options: [],
    };

    // TODO: Move this to a parse file function
    while (!isDone(context.stream)) {
        try {
            const d = parseDeclaration(context);
            context.declarations.push(d);
        } catch (e) {
            if (!ParseError.is(e)) throw e;
            pushError(e);
            // TODO: Enter panic mode
            // for now just quit
            break;
        }
    }

    if (getErrors().length === 0) {
        try {
            const { declarations, vars, options } = context;
            // Resolve the variable
            vars.forEach((v) => {
                v["_value"] = resolveIdentifier(v.identifier, declarations);
            });

            // Resolve the options
            options.forEach((o) => resolveOption(o, declarations));
        } catch (e) {
            if (!ResolutionError.is(e)) throw e;
            pushError(e);
        }
    }

    return new File(context.declarations);
}

const parseDeclaration: ParseFunc<Declaration> = (context) => {
    // declaration -> IDENTIFIER "=" value NEWLINE | EOF

    let comment: string | null = null;
    if (isComment(peek(context.stream))) comment = advance(context.stream).str;

    const identifier = advanceIf(
        context.stream,
        isIdentifier,
        expected("a declaration name")
    );

    if (isReserved(identifier.str))
        error(
            `${identifier.str} is reserved. You can't use it to name things.`,
            identifier
        );

    advanceIf(context.stream, isEquals, expected("="));
    const value = parseValue(context);

    return new Declaration(identifier, value, comment);
};

const parseField: ParseFunc<Field> = (context) => {
    // field -> (COMMENT)? IDENTIFIER "=" value | var
    let comment: string | null = null;
    if (isComment(peek(context.stream))) {
        comment = advance(context.stream).str;
    }

    const identifier = advanceIf(
        context.stream,
        isIdentifier,
        expected("a field name")
    );

    let isOptional = false;
    const optionalToken = peek(context.stream);
    if (isQuestion(optionalToken)) {
        isOptional = true;
        advance(context.stream);
    }

    advanceIf(context.stream, isColon, expected(":"));
    const next = peek(context.stream);
    let value: Id | Value | VarName;
    if (isId(next)) {
        value = parseId(context);
    } else {
        value = parseValue(context);
    }

    return new Field(identifier, value, comment, isOptional);
};

const parseVarName: ParseFunc<VarName> = (context) => {
    const token = advance(context.stream);
    if (!isIdentifier(token)) {
        throw new ParseError(
            `Expected an identifier but received: ${token.type}`,
            token
        );
    }
    const v = new VarName(token);
    context.vars.push(v);
    return v;
};

const parseValue: ParseFunc<Value | VarName> = (context) => {
    // value -> type | literal

    const token = peek(context.stream);

    switch (token.type) {
        // Literals
        case TokenType.STR:
            return parseStringLiteral(context);
        case TokenType.NUM:
            return parseNumberLiteral(context);
        case TokenType.BOOL:
            return parseBoolLiteral(context);
        default:
            return parseType(context);
    }
};

const parseType: ParseFunc<Type | VarName> = (context) => {
    const t = peek(context.stream);
    switch (t.type) {
        // Objects
        case TokenType.OPEN_BRACE:
            return parseObject(context);
        // Range
        case TokenType.OPEN_BRACKET:
            return parseRange(context);
        // Primitives & References
        case TokenType.IDENTIFIER:
            switch (t.str) {
                // Lists
                case Reserved.List:
                    return parseList(context);
                // Set
                case Reserved.Set:
                    return parseSet(context);
                // Map
                case Reserved.Map:
                    return parseMap(context);

                // Primitives
                case Reserved.Bool:
                    return parseBool(context);
                case Reserved.Number:
                    return parseNumber(context);
                case Reserved.String:
                    return parseString(context);
                case Reserved.Url:
                    return parseUrl(context);
                case Reserved.Image:
                    return parseImage(context);
                case Reserved.Video:
                    return parseVideo(context);
                case Reserved.Sound:
                    return parseSound(context);
                // Option
                case Reserved.Option:
                    return parseOption(context);
                default:
                    // assume it's a variable name. The resolver will flag this as an error if its not
                    return parseVarName(context);
            }
        default:
            throw new ParseError(
                `Could not generate type with ${printToken(t)}`,
                t
            );
    }
};

const parseRange: ParseFunc<Range> = (context) => {
    advanceIf(context.stream, isOpenBracket, expected("["));
    let low = parseNumberLiteral(context);
    let high = parseNumberLiteral(context);
    if (low.value > high.value) {
        const t = low;
        low = high;
        high = t;
    }
    advanceIf(context.stream, isClosedBracket, expected("]"));
    return new Range(low.value, high.value);
};

const parseMap: ParseFunc<GMLMap> = (context) => {
    // map -> "map" "(" STRING | reference TYPE ")"
    advanceIf(
        context.stream,
        (t) => t.str === Reserved.Map,
        expected("the map keyword")
    );
    advanceIf(context.stream, isOpenParen, expected("("));
    let keyType: Str | Option;
    if (test(context.stream, (t) => t.str === Reserved.String)) {
        keyType = parseString(context);
    } else {
        keyType = parseOption(context);
    }
    const valueType = parseType(context);
    advanceIf(context.stream, isClosedParen, expected(")"));
    return new GMLMap(keyType, valueType);
};

const parseSet: ParseFunc<GMLSet> = (context) => {
    advanceIf(
        context.stream,
        (t) => t.str === Reserved.Set,
        expected("the set keyword")
    );
    advanceIf(context.stream, isOpenParen, expected("("));
    const values: string[] | number[] = [];
    if (test(context.stream, isNumLiteral)) {
        while (
            !isDone(context.stream) &&
            !test(context.stream, isClosedParen)
        ) {
            const literal = advanceIf(
                context.stream,
                isNumLiteral,
                expected("a number")
            );
            const numValue = getNumValue(literal);
            (values as number[]).push(numValue);
        }
    } else if (test(context.stream, isStrLiteral)) {
        while (
            !isDone(context.stream) &&
            !test(context.stream, isClosedParen)
        ) {
            const literal = advanceIf(
                context.stream,
                isStrLiteral,
                expected("a string")
            );
            (values as string[]).push(literal.str);
        }
    } else {
        throw new ParseError(
            `Expected a numeric or string literal.`,
            peek(context.stream)
        );
    }
    advanceIf(context.stream, isClosedParen, expected(")"));
    return new GMLSet(values);
};

const parseNumberLiteral: ParseFunc<NumLiteral> = (context) => {
    const num = advanceIf(context.stream, isNumLiteral, expected("a number"));
    const val = getNumValue(num);
    return new NumLiteral(val);
};

const parseStringLiteral: ParseFunc<StrLiteral> = (context) => {
    const str = advanceIf(context.stream, isStrLiteral, expected("a string"));
    return new StrLiteral(str.str);
};

const parseBoolLiteral: ParseFunc<BoolLiteral> = (context) => {
    const bool = advanceIf(
        context.stream,
        isBoolLiteral,
        expected("true/false")
    );
    const val = getBoolValue(bool);
    return new BoolLiteral(val);
};

const parseObject: ParseFunc<Obj> = (context) => {
    const fields: Field[] = [];
    const openingBrace = advanceIf(context.stream, isOpenBrace, expected("{"));
    let hasId = false;
    while (!isDone(context.stream)) {
        if (test(context.stream, isClosedBrace)) {
            advance(context.stream);
            return new Obj(fields);
        }
        const field = parseField(context);
        if (field.isId && hasId) {
            throw new ParseError(
                `Duplicate identifier field. Objects can only have one identifier field`,
                field.identifier
            );
        } else {
            hasId = true;
        }
        fields.push(field);
    }
    throw new ParseError("Could not find closing brace", openingBrace);
};

const parseList: ParseFunc<List> = (context) => {
    // list -> "list" "(" (type | IDENTIFIER) (NUMBER)? ")"
    let size: number | Range | VarName | null = null;

    advanceIf(
        context.stream,
        (t) => t.str === Reserved.List,
        (t) => {
            throw new ParseError(
                `Expected the list keyword but received ${t.str}`,
                t
            );
        }
    );
    const openingParen = advanceIf(context.stream, isOpenParen, expected("("));

    let type: Type | VarName | undefined;

    // parse it as a type
    if (type === undefined) {
        type = parseType(context);
    }

    const sizeToken = peek(context.stream);
    if (isIdentifier(sizeToken)) {
        // check if it's a variable
        size = parseVarName(context);
    }

    // Check to see if it's a number or a range
    if (size === null) {
        if (test(context.stream, isNumLiteral)) {
            size = parseNumberLiteral(context).value;
        } else if (test(context.stream, isOpenBracket)) {
            size = parseRange(context);
        }
    }

    advanceIf(context.stream, isClosedParen, () => {
        error("Could not find closing parenthesis for list", openingParen);
    });
    return new List(type, size);
};

const parseId: ParseFunc<Id> = (context) => {
    // The parseValue function already validated the identifier
    advance(context.stream);
    // check if there's a default value
    const defaultValue = parseDefaultValue(
        context,
        isStrLiteral,
        getStringValue
    );
    return new Id(defaultValue);
};

const isLiteral = <T extends string | number | boolean>(
    val: any
): val is LiteralValue<T> => {
    return BoolLiteral.is(val) || NumLiteral.is(val) || StrLiteral.is(val);
};

const parseDefaultValue = <T extends string | number | boolean>(
    context: ParseContext,
    tokenPredicate: (token: Token) => boolean,
    tokenTransformer: (token: Token) => T
): T => {
    const openParen = advance(context.stream);
    if (!isOpenParen(openParen))
        throw new ParseError(
            `Default values must have an opening parenthesis.`,
            openParen
        );

    let result: T;

    const t = advance(context.stream);
    if (tokenPredicate(t)) {
        result = tokenTransformer(t);
    } else if (isIdentifier(t)) {
        // try to resolve this variable as a literal
        const val = resolveTo<LiteralValue<T>>(isLiteral, t, context);
        result = val.value;
    } else {
        throw new ParseError(`Could not parse default value`, t);
    }

    const closedParen = advance(context.stream);
    if (!isClosedParen(closedParen))
        throw new ParseError(
            `Default values must have a closing parenthesis.`,
            closedParen
        );

    return result;
};

type PrimitiveCtor =
    | typeof Num
    | typeof Str
    | typeof Bool
    | typeof Video
    | typeof Image
    | typeof Sound;
type PrimitiveCtorParam = ConstructorParameters<PrimitiveCtor>[0];

const primitiveParser = <P extends string | boolean | number>(
    ctr: new (p: any) => PrimitiveValue<P>,
    tokenPredicate: (token: Token) => boolean,
    tokenTransformer: (token: Token) => P
): ParseFunc<PrimitiveValue<P>> => (context) => {
    // The parseValue function already validated the identifier
    advance(context.stream);
    // check if there's a default value
    const defaultValue: PrimitiveCtorParam = parseDefaultValue(
        context,
        tokenPredicate,
        tokenTransformer
    );
    return new ctr(defaultValue);
};

const parseNumber = primitiveParser(Num, isNumLiteral, getNumValue);

const parseBool = primitiveParser(Bool, isBoolLiteral, getBoolValue);

const getStringValue = (t: Token) => t.str;
const parseString = primitiveParser(Str, isStrLiteral, getStringValue);
const parseUrl = primitiveParser(Url, isStrLiteral, getStringValue);
const parseVideo = primitiveParser(Video, isStrLiteral, getStringValue);
const parseImage = primitiveParser(Image, isStrLiteral, getStringValue);
const parseSound = primitiveParser(Sound, isStrLiteral, getStringValue);

const parseOption: ParseFunc<Option> = ({ stream, options }) => {
    // path -> var | var"."path
    // reference -> ref(path)

    advanceIf(
        stream,
        (t) => t.str === Reserved.Option,
        (t) => error("References must start with keyword 'ref'.", t)
    );

    advanceIf(stream, isOpenParen, expected("("));

    let t: Token = advanceIf(stream, isIdentifier, (t) =>
        error(`The first token in a path must be an identifier`, t)
    );
    const tokens: Token[] = [t];

    while (!isClosedParen((t = advance(stream)))) {
        if (isPeriod(t)) {
            const next = peek(stream);
            if (isIdentifier(next)) continue;
            else
                error(`Periods in paths must be followed by identifiers`, next);
        }
        if (isIdentifier(t)) {
            tokens.push(t);
        } else {
            error(
                `Paths can only contain identifiers and periods, received: ${t.type}`,
                t
            );
        }
    }
    if (tokens.length < 2)
        error(
            `Paths must have at least two elements. The first must be a top-level object declaration`,
            tokens[tokens.length - 1]
        );

    const ref = new Option(tokens);
    options.push(ref);
    return ref;
};

///////////////////////////////////////////////////////////////////////////////
// RESOLUTION
///////////////////////////////////////////////////////////////////////////////

const lookupDecl = (identifier: Token, decls: Declaration[]) => {
    for (let i = 0; i < decls.length; i++) {
        if (decls[i].identifier.str === identifier.str) {
            return decls[i];
        }
    }
    throw new ResolutionError(
        `Could not resolve ${identifier.str}`,
        identifier
    );
};

function resolveTo<T>(
    typePredicate: (val: any) => val is T,
    identifier: Token,
    context: ParseContext
): T {
    const val = resolveIdentifier(identifier, context.declarations);
    if (!typePredicate(val))
        throw new ResolutionError(
            `Expected ${identifier.str} to resolve to a different value than ${val.type}`,
            identifier
        );
    return val as T;
}

function resolveIdentifier(identifier: Token, decls: Declaration[]) {
    if (!isIdentifier(identifier))
        error(`Expected an identifier but got: ${identifier.type}`, identifier);
    // NOTE: We can assume identifiers always point to declarations because variables can only be declared in the global scope
    let decl = lookupDecl(identifier, decls);
    const visited = new Set<string>(identifier.str);
    while (VarName.is(decl.value)) {
        if (visited.has(decl.value.identifier.str))
            throw new ResolutionError(
                `Trying to resolve a cyclical reference: ${decl.value.identifier.str}`,
                decl.value.identifier
            );
        visited.add(decl.value.identifier.str);
        decl = lookupDecl(decl.value.identifier, decls);
    }
    return decl.value;
}

function resolveField(identifier: Token, obj: Obj) {
    if (!isIdentifier(identifier)) error("Expected an identifier", identifier);
    const field = obj.fields.find((f) => f.identifier.str === identifier.str);
    if (field === undefined)
        throw new ResolutionError(
            `${identifier.str} is not a valid field name.`,
            identifier
        );
    return field;
}

function resolveOption(option: Option, decls: Declaration[]) {
    const path = option.path;
    const root = lookupDecl(path[0], decls);
    if (root === undefined)
        throw new ResolutionError(
            `Cannot resolve root of option path`,
            path[0]
        );
    if (!Obj.is(root.value))
        throw new ResolutionError(
            `Option path starts with something other than an object`,
            path[0]
        );
    let obj = root.value;

    for (let i = 1; i < path.length - 1; i++) {
        const field = resolveField(path[i], obj);

        let value = field.value;
        if (Obj.is(value)) {
            obj = value;
        } else if (List.is(value)) {
            // if the parameter is a variable, resolve it
            if (VarName.is(value.typeParam)) {
                value = resolveIdentifier(value.typeParam.identifier, decls);
            }
            if (!Obj.is(value))
                throw new ResolutionError(
                    `Path elements that are lists must be composed of objects`,
                    path[i]
                );
            obj = value;
        } else {
            throw new ResolutionError(
                `Internal path elements must be either lists of objects or objects`,
                path[i]
            );
        }
    }

    // The last element must point to a list of objects
    const lastPathElement = path[path.length - 1];
    let list = resolveField(lastPathElement, obj).value;
    if (!List.is(list))
        throw new ResolutionError(
            `Last element in an option path must be a list`,
            lastPathElement
        );

    let typeParam = list.typeParam;
    if (VarName.is(typeParam)) {
        const resolved = resolveIdentifier(typeParam.identifier, decls);
        if (!Obj.is(resolved))
            throw new ResolutionError(
                `Last element in an option path must be a list of objects`,
                lastPathElement
            );
        typeParam = resolved;
    } else {
        if (!Obj.is(typeParam))
            throw new ResolutionError(
                `Last element in an option path must be a list of objects`,
                lastPathElement
            );
    }

    option["_object"] = typeParam;
    option["_list"] = list;
}
