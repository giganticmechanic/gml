import { Token } from "./token";

const errors: GMLError[] = [];
export const getErrors = () => errors.slice(0);

export function pushError(err: GMLError) {
    errors.push(err);
}

export const clearErrors = () => {
    errors.length = 0;
};

export enum ErrorType {
    InvalidToken,
    UnterminatedString,
    UnterminatedComment,
    ParseError,
    ResolutionError,
}

export abstract class GMLError {
    protected constructor(
        public readonly type: ErrorType,
        public readonly info: string,
        public readonly description: string,
        public readonly offset: number
    ) {}
}

export class InvalidTokenError extends GMLError {
    static readonly description = "Invalid Token";

    static is(arg: any): arg is InvalidTokenError {
        return arg.constructor === InvalidTokenError;
    }

    constructor(message: string, offset: number) {
        super(
            ErrorType.InvalidToken,
            message,
            InvalidTokenError.description,
            offset
        );
    }
}

export class UnterminatedStringError extends GMLError {
    static readonly description = `No closing quote`;

    static is(arg: any): arg is UnterminatedStringError {
        return arg.constructor === UnterminatedStringError;
    }

    constructor(offset: number) {
        super(
            ErrorType.UnterminatedString,
            `All strings need a closing quote (")`,
            UnterminatedStringError.description,
            offset
        );
    }
}

export class UnterminatedCommentError extends GMLError {
    static readonly description = "No closing >>";

    static is(arg: any): arg is UnterminatedCommentError {
        return arg.constructor === UnterminatedCommentError;
    }

    constructor(offset: number) {
        super(
            ErrorType.UnterminatedComment,
            "All comments need a closing >>",
            UnterminatedCommentError.description,
            offset
        );
    }
}

export class ParseError extends GMLError {
    static readonly description = "Unparseable token";

    static is(arg: any): arg is ParseError {
        return arg.constructor === ParseError;
    }
    constructor(message: string, token: Token) {
        super(
            ErrorType.ParseError,
            message,
            ParseError.description,
            token.offset
        );
    }
}

export class ResolutionError extends GMLError {
    static readonly description = "Cannot resolve name";

    static is(arg: any): arg is ResolutionError {
        return arg.constructor === ResolutionError;
    }
    constructor(message: string, token: Token) {
        super(
            ErrorType.ResolutionError,
            message,
            ParseError.description,
            token.offset
        );
    }
}

export type ErrorContextInfo = {
    line: number;
    column: number;
    message: string;
    context: string | null;
};
export function getErrorInfo(
    errors: GMLError[],
    input: string
): ErrorContextInfo[] {
    // split the input
    const lines: string[] = input.split("\n");

    const getLocation = (offset: number): { line: number; column: number } => {
        const o = offset;
        if (o !== -1) {
            let c = -1;
            for (let i = 0; i < lines.length; i++) {
                const len = lines[i].length;
                c += len + 1;
                if (o <= c) {
                    return {
                        line: i + 1,
                        column: o - (c - len),
                    };
                }
            }
        }
        return { line: -1, column: -1 };
    };

    const createInfo = (err: GMLError): ErrorContextInfo => {
        const { line, column } = getLocation(err.offset);
        if (line === -1) {
            return {
                message: `${err.description}: ${err.info}`,
                context: null,
                line: -1,
                column: -1,
            };
        } else {
            const errorLine = lines[line - 1];
            return {
                context: errorLine,
                message: "[" + err.description + "] " + err.info,
                line: line,
                column: column,
            };
        }
    };

    return errors.map(createInfo);
}
