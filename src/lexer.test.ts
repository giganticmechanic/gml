import { clearErrors, getErrors } from "./error";
import { lex } from "./lexer";
import "./util/jest-helpers";
import { TokenType } from "./token-type";

describe("Lexer", () => {
    afterEach(clearErrors);

    describe("Single Characters", () => {
        it("Recognizes ,", () => {
            const tokens = lex("blarg ,");
            expect(getErrors()).toHaveLength(0);
            expect(tokens).toContainObject({ type: TokenType.COMMA });
        });
        it("Recognizes *", () => {
            const tokens = lex("blarg *");
            expect(getErrors()).toHaveLength(0);
            expect(tokens).toContainObject({ type: TokenType.STAR });
        });
        it("Recognizes ()", () => {
            const tokens = lex("blarg()");
            expect(getErrors()).toHaveLength(0);
            expect(tokens).toContainObject({ type: TokenType.OPEN_PAREN });
            expect(tokens).toContainObject({ type: TokenType.CLOSED_PAREN });
        });
        it("Recognizes {}", () => {
            const tokens = lex("{}");
            expect(getErrors()).toHaveLength(0);
            expect(tokens).toContainObject({ type: TokenType.OPEN_BRACE });
            expect(tokens).toContainObject({ type: TokenType.CLOSED_BRACE });
        });
        it("Recognizes []", () => {
            const tokens = lex("[]");
            expect(getErrors()).toHaveLength(0);
            expect(tokens).toContainObject({ type: TokenType.OPEN_BRACKET });
            expect(tokens).toContainObject({ type: TokenType.CLOSED_BRACKET });
        });
        it("Recognizes true", () => {
            const tokens = lex("true");
            expect(getErrors()).toHaveLength(0);
            expect(tokens).toContainObject({
                type: TokenType.BOOL,
                str: "true",
            });
        });
        it("Recognizes false", () => {
            const tokens = lex("false");
            expect(getErrors()).toHaveLength(0);
            expect(tokens).toContainObject({
                type: TokenType.BOOL,
                str: "false",
            });
        });
        it("Recognizes id", () => {
            const tokens = lex("id");
            expect(getErrors()).toHaveLength(0);
            expect(tokens).toContainObject({
                type: TokenType.ID,
                str: "id",
            });
        });
        it("Recognizes question mark", () => {
            const tokens = lex("?");
            expect(getErrors()).toHaveLength(0);
            expect(tokens).toContainObject({
                type: TokenType.QUESTION,
                str: "?",
            });
        });
        it("Recognizes :", () => {
            const tokens = lex(":");
            expect(getErrors()).toHaveLength(0);
            expect(tokens).toContainObject({ type: TokenType.COLON, str: ":" });
        });
        it("Recognizes =", () => {
            const tokens = lex("=");
            expect(getErrors()).toHaveLength(0);
            expect(tokens).toContainObject({ type: TokenType.EQUALS });
        });
        it("Ignores tabs", () => {
            const tokens = lex("{\t}");
            expect(getErrors()).toHaveLength(0);
            expect(tokens).toHaveLength(3);
        });
        it("Ignores spaces", () => {
            const tokens = lex("{ }");
            expect(getErrors()).toHaveLength(0);
            expect(tokens).toHaveLength(3);
        });
    });

    describe("Comments", () => {
        it("Recognizes comments", () => {
            const content = "Hello";
            const tokens = lex("<<" + content + ">>");
            expect(getErrors()).toHaveLength(0);
            expect(tokens).toContainObject({
                type: TokenType.COMMENT,
                str: content,
            });
        });
        it("Does not drop out of lexing if comment contains single >", () => {
            const content = "(>.>)";
            lex("<<" + content + ">>");
            expect(getErrors()).toHaveLength(0);
        });
        it("Errors if partially uninitiated", () => {
            const content = "Hello";
            lex("<" + content + ">>");
            expect(getErrors()).toHaveLength(1);
        });
        it("Errors if partially unterminated", () => {
            const content = "Hello";
            const tokens = lex("<<" + content + ">");
            expect(getErrors()).toHaveLength(1);
        });
        it("Errors if unterminated", () => {
            const content = "Hello";
            const tokens = lex("<<" + content);
            expect(getErrors()).toHaveLength(1);
        });
    });

    describe("Literals", () => {
        it("Recognizes integers", () => {
            const answer = "42";
            const tokens = lex(`${answer}`);
            expect(getErrors()).toHaveLength(0);
            expect(tokens).toContainObject({
                type: TokenType.NUM,
                str: answer,
            });
        });
        it("Recognizes floats", () => {
            const pi = "3.14159265359";
            const tokens = lex(`${pi}`);
            expect(getErrors()).toHaveLength(0);
            expect(tokens).toContainObject({ type: TokenType.NUM, str: pi });
        });
        it("Recognizes strings", () => {
            const string = "blargh";
            const tokens = lex(`"${string}"`);
            expect(getErrors()).toHaveLength(0);
            expect(tokens).toContainObject({
                type: TokenType.STR,
                str: string,
            });
        });
        it("Errors on unterminated strings", () => {
            const string = "blargh";
            lex(`"${string}`);
            expect(getErrors()).toHaveLength(1);
        });
    });

    describe("Position", () => {
        it("Calculates offset", () => {
            const tokens = lex('abc("def")');
            expect(getErrors()).toHaveLength(0);
            // abc
            expect(tokens[0].offset).toEqual(0);
            // (
            expect(tokens[1].offset).toEqual(3);
            // "def"
            expect(tokens[2].offset).toEqual(4);
            // )
            expect(tokens[3].offset).toEqual(9);
        });
    });

    describe("General", () => {
        it("Inserts an EOF token", () => {
            const tokens = lex("123");
            expect(tokens).toContainObject({ type: TokenType.EOF });
        });
        it("Throws on empty input", () => {
            expect(() => lex("")).toThrow();
        });
        it("Throws on all whitspace input", () => {
            expect(() => lex("\t\t  \t\t")).toThrow();
        });
        it("Errors on invalid input", () => {
            lex("^");
            expect(getErrors()).toHaveLength(1);
        });
    });
});
