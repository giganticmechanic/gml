const { readFile, writeFile } = require("fs");
const { promisify } = require("util");
const path = require("path");

const assetsPath = path.join(__dirname, "assets");
const descriptionPath = path.join(assetsPath, "description.md");
const lexicalPath = path.join(assetsPath, "lexical-grammar.md");
const syntaxPath = path.join(assetsPath, "syntax-grammar.md");
const examplePath = path.join(assetsPath, "test.gml");

const read = promisify(readFile);
const write = promisify(writeFile);

let description, lexical, syntax, example;

const readme = () => `#GML

##DESCRIPTION
${description}

##LEXICAL GRAMMAR
${lexical}

##SYNTAX GRAMMAR
${syntax}

##EXAMPLE
\`\`\`
${example}
\`\`\`
`;

Promise.all([
    read(descriptionPath).then(buffer => (description = buffer.toString())),
    read(lexicalPath).then(buffer => (lexical = buffer.toString())),
    read(syntaxPath).then(buffer => (syntax = buffer.toString())),
    read(examplePath).then(buffer => (example = buffer.toString()))
])
    .then(() => write(path.join(__dirname, "README.md"), readme()))
    .catch(err => console.log(err));
