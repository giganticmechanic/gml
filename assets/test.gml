<<The default image (this should be some kind of blank "error" image)>>
DefaultImage = image("http://no-image.jpg")

<<The default image (this should be some kind of blank "error" video)>>
DefaultVideo = video("http://no-video.mp4")

<<One of the proposals to address one of the issues>>
Proposal = {
    title: id("A unique proposal name")
    summary: "I'm the summary for the proposal"
    arguments: map(
        option(CoreScenario.data.coalitions.identities)
        list(string("An Argument"))
    )
}

<<One of the three underlying issues of the scenario>>
Issue = {
    name: id("A unique issue name")
    image: DefaultImage
    description: "I'm the description for this issue"
    proposals: list(Proposal 3)
}

<<A group with a shared set of of opinions on the issues>>
Identity = {
    name: id("A unique identity name")
    want: "Cookies"
    fear: "Kale"
    article:set("a" "an")
    color: "bg-red-30"
    leftImage: DefaultImage
    rightImage: DefaultImage
    identityImage: DefaultImage
    icon: DefaultImage
    reactions: map(
        option(CoreScenario.data.issues)
        list(string("A reaction to the issue") 2)
    )
}

<<A player profile>>
Profile = {
    identity: option(CoreScenario.data.coalitions.identities)
    issueGroup: option(CoreScenario.data.issues)
    isUnique: false
    bio: "Jenny forgot to create a bio for you..."
}

<<A set of identities with overlapping opinions>>
Coalition = {
    name: id("A unique coalition name")
    description: "A description of the coalition"
    image: DefaultImage
    identities: list(Identity)
    color:string("bg-red-100")
    proposals:list(option(CoreScenario.data.issues.proposals))
}

<<Questions associated with a page>>
Questions = list(string("question"))

<<A page with no scenario specific static content>>
ProceduralPage = {}

<<A page with questions>>
QuestionsPage = {
    questions: Questions
}

<<A page with a video on the display>>
VideoPage = {
    video: DefaultVideo
}

<<A page with a video on the display>>
VideoPageWithQuestions = {
    questions: Questions
    video: DefaultVideo
}

<<The list of pages in a scenario>>
Pages = {
    assignProfiles: ProceduralPage
    introduction: VideoPage
    scenarioIntro: VideoPageWithQuestions
    reviewProfile: ProceduralPage
    talkToNeighbor: ProceduralPage
    issue1: VideoPageWithQuestions
    issue2: VideoPageWithQuestions
    issue3: VideoPageWithQuestions
    proposalInstruction: VideoPage
    exampleProposal: {
        exampleProposal: option(CoreScenario.data.issues.proposals)
        questions: list(string("A question about the proposal"))
    }
    groupReviewInstruction: VideoPage
    groupReview: ProceduralPage
    compromiseInstruction: VideoPage
    committees: ProceduralPage
    compromise: ProceduralPage
    voteInstruction: VideoPage
    voteIntro: ProceduralPage
    vote1: QuestionsPage
    debate1: ProceduralPage
    vote2: QuestionsPage
    debate2: ProceduralPage
    vote3: QuestionsPage
    debate3: ProceduralPage
    finalTally: QuestionsPage
    epilogue: VideoPage
    discussion: {
        discussionQuestions: list(string("A question about the experience"))
    }
}

<<The data for a scenario>>
CoreScenario = {
    _id: id("VP:CORE: Some unique name")
    name: string("The name of the scenario that will be displayed")
    tags: list(string("vp:core"))
    data: {
        coalitions: list(Coalition [2 999])
        issues: list(Issue 3)
        profiles: list(Profile [10 999])
        pages: Pages
    }
}