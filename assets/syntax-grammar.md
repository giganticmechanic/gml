```
file        -> (declaration)* EOF 
varName     -> IDENTIFIER // Must correspond to a declaration
declaration -> (COMMENT)? IDENTIFIER ":" value | varName
value       -> literal | type
path        -> varName"."path | varName // Must start with a top-level object declaration
option      -> option(path)   
literal     -> NUMBER | STRING | BOOL
type        -> object | primitive | list | range | option 
object      -> "{" field* "}"
id          -> "id" ("("STRING | varName")")?
field       -> (COMMENT)? varName ("?")? ":" value | varName | id
list        -> "list" "(" type | varName (NUMBER | range)? ")"
range       -> "[" NUMBER NUMBER "]"
set         -> "set" "(" NUMBER+ | STRING+ ")"
map         -> "map" "(" STRING | reference TYPE ")"
primitive   -> num | str | bool | url
num         -> "number" ("("NUMBER | varName")")?
str         -> "string" ("("STRING | varName")")?
bool        -> "bool" ("("BOOL | varName")")?
url         -> "url" ("("STRING | varName")")?
image       -> "image" ("("STRING | varName")")?
video       -> "video" ("("STRING | varName")")?
sound       -> "sound" ("("STRING | varName")")?
```