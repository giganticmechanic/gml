```
NUMBER      -> DIGIT+ ( "." DIGIT+ )? 
STRING      -> '"' <any char except '"'>* '"' 
BOOL        -> 'true' | 'false'
IDENTIFIER  -> ALPHA ( ALPHA | DIGIT )*
ALPHA       -> 'a' ... 'z' | 'A' ... 'Z' | '_' | '-'
DIGIT       -> '0' ... '9' 
COMMENT     -> '`' <any char except '`"`'>* '`'
```